﻿using System;
using System.Collections.Generic;
using System.IO;
using log4net;
using Quartz;

namespace ExamPuntoticket
{
    public class Class1 : IJob
    {
        private readonly ILog _log;

        public Class1(ILog log)
        {
            _log = log;
        }

        public void Execute(IJobExecutionContext context)
        {
            _log.Debug("Start process file");

            var l = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "code.txt"));
            var numerated = new List<string>();
            var notNumerated = new List<string>();
            var typeGroup = new Dictionary<string, int>();

            _log.Debug("Read code types");
            foreach (var s in l)
            {
                var data = s.Split(',');
                notNumerated.Add(data[0]);
                if (data[1] == "Cancha General")
                {
                    if (typeGroup.ContainsKey("Cancha General"))
                    {
                        typeGroup["Cancha General"] = typeGroup["Cancha General"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Cancha General", 1);
                    }
                }
                if (data[1] == "Galería")
                {
                    if (typeGroup.ContainsKey("Galería"))
                    {
                        typeGroup["Galería"] = typeGroup["Galería"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Galería", 1);
                    }
                }
                if (data[1] == "Vip")
                {
                    if (typeGroup.ContainsKey("Vip"))
                    {
                        typeGroup["Vip"] = typeGroup["Vip"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Vip", 1);
                    }
                }
                if (data[1] == "Palco")
                {
                    if (typeGroup.ContainsKey("Palco"))
                    {
                        typeGroup["Palco"] = typeGroup["Palco"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Palco", 1);
                    }
                }
            }

            _log.Debug("Save file numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "numerated_" + DateTime.Now.Ticks + ".txt")))
            {
                file.WriteLine("**** TICKETS NUMERADOS ****");
                foreach (var line in numerated)
                {
                    file.WriteLine(line);
                }
            }

            _log.Debug("Save file not numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "notnumerated_" + DateTime.Now.Ticks + ".txt")))
            {
                file.WriteLine("**** TICKETS NO NUMERADOS ****");
                foreach (var line in notNumerated)
                {
                    file.WriteLine(line);
                }
            }


            _log.Debug("Save file group for types");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "typegroup_" + DateTime.Now.Ticks + ".txt")))
            {
                file.WriteLine("**** CANTIDADES POR TIPOS DE TICKETS ****");
                foreach (var line in typeGroup)
                {
                    file.WriteLine(String.Format("{0} = {1}", line.Key, line.Value));
                }
            }
        }
    }
}