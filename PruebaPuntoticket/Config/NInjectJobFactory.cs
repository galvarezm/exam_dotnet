﻿using Ninject;
using Quartz;
using Quartz.Spi;

namespace ExamPuntoticket.Config
{
    public class NInjectJobFactory : IJobFactory
    {
        private readonly IKernel _kernel;

        public NInjectJobFactory(IKernel kernel)
        {
            _kernel = kernel;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            // GAM: Fix en el retorno del tipo de clase.
            return (IJob)_kernel.Get(bundle.JobDetail.JobType);
            //return _kernel.Get<IJob>();
        }

        public void ReturnJob(IJob job)
        {
            _kernel.Release(job);
        }
    }
}