﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExamPuntoticket.Test
{

    [TestClass]
    public class FileTest
    {
        /// <summary>
        /// GAM: Esta prueba unitaria verifica la existencia del archivo principal
        ///      antes de ser procesado por la aplicacion.
        /// </summary>
        [TestMethod]
        public void ExistsFileCodeTXT()
        {
            var fileTXT = Path.Combine(Directory.GetCurrentDirectory(), "code.txt");
            var result = File.Exists(fileTXT);
            Assert.AreEqual(true, result);
        }
    }
}